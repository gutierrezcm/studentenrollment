package com.studentenrollment;

import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import com.studentenrollment.handler.StudentEnrollmentHandler;
import com.studentenrollment.handler.StudentHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class RouterConfig {

    @Bean
    public RouterFunction<ServerResponse> routesStudent(StudentHandler handler) {
        return route(POST("/students"), handler::save)
                .andRoute(GET("/students/pageable"), handler::findByPage) //Hace conflicto con ruta 'students/{id}'
                .andRoute(GET("/students/parallel"), handler::findAllByAgeDescParallel)
                .andRoute(GET("/students/{id}"), handler::findById)
                .andRoute(GET("/students"), handler::findAllByAgeDesc)
                .andRoute(PUT("/students"), handler::modify)
                .andRoute(DELETE("/students/{id}"), handler::delete);
    }

    @Bean
    public RouterFunction<ServerResponse> reoutesStudentEnrollment(StudentEnrollmentHandler handler) {
        return route(POST("/student-enrollment"), handler::save)
                .andRoute(GET("/student-enrollment"), handler::findAll)
                .andRoute(GET("/student-enrollment/pageable"), handler::findByPage) //Hace conflicto con ruta 'student-enrollment/{id}'
                .andRoute(GET("/student-enrollment/{id}"), handler::findById)
                .andRoute(PUT("/student-enrollment"), handler::modify)
                .andRoute(DELETE("/student-enrollment/{id}"), handler::delete);
    }

}
