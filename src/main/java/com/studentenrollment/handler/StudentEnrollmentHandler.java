package com.studentenrollment.handler;

import com.studentenrollment.document.StudentEnrollment;
import com.studentenrollment.service.IStudentEnrollmentService;
import com.studentenrollment.validators.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class StudentEnrollmentHandler {

    @Autowired
    private IStudentEnrollmentService studentEnrollmentService;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> save(ServerRequest request) {
        return request.bodyToMono(StudentEnrollment.class)
                .flatMap(this.requestValidator::validar)
                .flatMap(studentEnrollmentService::save)
                .flatMap(studentEnrollment -> ServerResponse.created(URI.create(request.uri().toString().concat("/")
                        .concat(studentEnrollment.getId())))
                        .contentType(MediaType.APPLICATION_STREAM_JSON)
                        .body(fromValue(studentEnrollment))
                );
    }

    public Mono<ServerResponse> findAll(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(studentEnrollmentService.findAll(), StudentEnrollment.class);
    }

    public Mono<ServerResponse> findByPage(ServerRequest request) {
        return Mono.fromCallable(() -> {
            int page = request.queryParam("page").map(s -> Integer.parseInt(s)).orElse(0);
            int size = request.queryParam("size").map(s -> Integer.parseInt(s)).orElse(10);
            System.out.println("page=" + page + ", size=" + size);
            return PageRequest.of(page, size);
        }).flatMap(studentEnrollmentService::findByPage)
                .flatMap(studentPageSupport -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_STREAM_JSON)
                        .body(fromValue(studentPageSupport)));
    }

    public Mono<ServerResponse> findById(ServerRequest request) {
        return studentEnrollmentService.findById(request.pathVariable("id"))
                .flatMap(studentEnrollment -> ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_STREAM_JSON)
                                .body(fromValue(studentEnrollment)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> modify(ServerRequest request) {
        return null;
    }

    public Mono<ServerResponse> delete(ServerRequest request) {
        return studentEnrollmentService.findById(request.pathVariable("id"))
                .flatMap(studentEnrollment -> studentEnrollmentService.remove(studentEnrollment.getId())
                                                .then(ServerResponse.noContent().build()))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

}
