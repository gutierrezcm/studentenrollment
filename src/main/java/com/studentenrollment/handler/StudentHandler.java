package com.studentenrollment.handler;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import com.studentenrollment.document.Student;
import com.studentenrollment.service.IStudentService;
import com.studentenrollment.validators.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.net.URI;
import java.util.Comparator;

@Component
public class StudentHandler {

    @Autowired
    private IStudentService studentService;

    @Autowired
    private RequestValidator requestValidator;


    public Mono<ServerResponse> save(ServerRequest request) {
        return request.bodyToMono(Student.class)
                .flatMap(this.requestValidator::validar)
                .flatMap(studentService::save)
                .flatMap(student -> ServerResponse.created(URI.create(request.uri().toString().concat("/").concat(student.getId())))
                    .contentType(MediaType.APPLICATION_STREAM_JSON)
                        .body(fromValue(student))
                );
    }

    public Mono<ServerResponse> findAllByAgeDesc(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(studentService.findAll()
                        .sort(Comparator.comparing(Student::getAge, Comparator.reverseOrder()))
                        , Student.class);
    }

    public Mono<ServerResponse> findAllByAgeDescParallel(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(studentService.findAll()
                        .parallel()
                        .runOn(Schedulers.elastic())
                        .sorted(Comparator.comparing(Student::getAge, Comparator.reverseOrder()))
                        , Student.class);
    }

    public Mono<ServerResponse> findByPage(ServerRequest request) {
        return Mono.fromCallable(() -> {
            int page = request.queryParam("page").map(s -> Integer.parseInt(s)).orElse(0);
            int size = request.queryParam("size").map(s -> Integer.parseInt(s)).orElse(10);
            System.out.println("page=" + page + ", size=" + size);
            return PageRequest.of(page, size);
        }).flatMap(studentService::findByPage)
                .flatMap(studentPageSupport -> ServerResponse.ok()
                                                .contentType(MediaType.APPLICATION_STREAM_JSON)
                                                .body(fromValue(studentPageSupport)));
    }

    public Mono<ServerResponse> findById(ServerRequest request) {
        return studentService.findById(request.pathVariable("id"))
                .flatMap(student -> ServerResponse.ok()
                                        .contentType(MediaType.APPLICATION_STREAM_JSON)
                                        .body(fromValue(student)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> modify(ServerRequest request) {
        return request.bodyToMono(Student.class)
                .flatMap(requestValidator::validar)
                .flatMap(studentService::update)
                .flatMap(student -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_STREAM_JSON)
                        .body(fromValue(student)))
                .switchIfEmpty(ServerResponse
                        .notFound()
                        .build());
    }

    public Mono<ServerResponse> delete(ServerRequest request) {
        return studentService.findById(request.pathVariable("id"))
                .flatMap(student -> studentService.remove(student.getId())
                                        .then(ServerResponse.noContent().build()))
                .switchIfEmpty(ServerResponse.notFound().build());

    }

}
