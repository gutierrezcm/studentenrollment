package com.studentenrollment.repository;

import com.studentenrollment.document.UserDb;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface IUserRepo extends ReactiveMongoRepository<UserDb, String> {

    Mono<UserDb> findOneByUser(String user);
}
