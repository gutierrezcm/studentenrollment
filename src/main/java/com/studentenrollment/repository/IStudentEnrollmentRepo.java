package com.studentenrollment.repository;

import com.studentenrollment.document.StudentEnrollment;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface IStudentEnrollmentRepo extends ReactiveMongoRepository<StudentEnrollment, String> {
}