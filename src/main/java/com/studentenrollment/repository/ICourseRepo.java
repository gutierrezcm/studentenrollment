package com.studentenrollment.repository;

import com.studentenrollment.document.Course;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ICourseRepo extends ReactiveMongoRepository<Course, String> {
}
