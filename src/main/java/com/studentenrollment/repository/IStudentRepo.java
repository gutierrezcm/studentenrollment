package com.studentenrollment.repository;

import com.studentenrollment.document.Student;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface IStudentRepo extends ReactiveMongoRepository<Student, String> {
}
