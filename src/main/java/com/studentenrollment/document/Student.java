package com.studentenrollment.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;

@Document(collection = "students")
public class Student {

    @Id
    private String id;

    @NotEmpty
    @Size(min = 5, max = 75)
    private String names;

    @NotEmpty
    @Size(min = 5, max = 75)
    private String lastName;

    @NotEmpty
    @Pattern(regexp = "^[0-9]{8}")
    private String documentNumber;

    @Min(value = 6, message = "Age should not be less than 6")
    @Max(value = 110, message = "Age should not be greater than 110")
    private double age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }
}
