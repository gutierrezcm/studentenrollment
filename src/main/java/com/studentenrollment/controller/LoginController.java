package com.studentenrollment.controller;

import com.studentenrollment.security.AuthRequest;
import com.studentenrollment.security.AuthResponse;
import com.studentenrollment.security.ErrorLogin;
import com.studentenrollment.security.JwtUtil;
import com.studentenrollment.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
public class LoginController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private IUserService userService;

    @PostMapping("/login")
    public Mono<ResponseEntity<?>> login(@RequestBody AuthRequest authRequest) {
        return userService.findByUsername(authRequest.getUsername())
                .map((userDetails) -> {
                    String token = jwtUtil.generateToken(userDetails);
                    Date expiration = jwtUtil.getExpirationDateFromToken(token);

                    if (BCrypt.checkpw(authRequest.getPassword(), userDetails.getPassword())) {
                        return ResponseEntity.ok(new AuthResponse(token, expiration));
                    } else {
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                                .body(new ErrorLogin("credenciales incorrectas", new Date()));
                    }
                }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping("/v2/login")
    public Mono<ResponseEntity<?>> login(@RequestHeader("usuario") String user,
                                         @RequestHeader("clave") String password) {
        return userService.findByUsername(user).map((userDetails) -> {
            String token = jwtUtil.generateToken(userDetails);
            Date expiracion = jwtUtil.getExpirationDateFromToken(token);

            if (BCrypt.checkpw(password, userDetails.getPassword())) {
                return ResponseEntity.ok(new AuthResponse(token, expiracion));
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                        .body(new ErrorLogin("credenciales incorrectas", new Date()));
            }
        }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

}
