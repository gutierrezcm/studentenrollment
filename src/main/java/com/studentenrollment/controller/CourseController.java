package com.studentenrollment.controller;

import com.studentenrollment.document.Course;
import com.studentenrollment.pagination.PageSupport;
import com.studentenrollment.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private ICourseService courseService;

    @PostMapping
    public Mono<ResponseEntity<Course>> save(@Valid @RequestBody Course course,
                                             final ServerHttpRequest request) {
        return courseService.save(course)
                .map(course1 -> ResponseEntity.created(URI.create(request.getURI().toString().concat("/")
                        .concat(course.getId())))
                        .body(course)
                );
    }

    @GetMapping
    public Mono<ResponseEntity<Flux<Course>>> listAll() {
        return Mono.fromCallable(() -> ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_STREAM_JSON)
                    .body(courseService.findAll()));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Course>> findById(@PathVariable("id") String id) {
        return courseService.findById(id)
                .map(course -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(course))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping
    public Mono<ResponseEntity<Course>> update(@Valid @RequestBody Course course) {
        return courseService.update(course)
                .map(course1 -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(course1))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
        return courseService.findById(id)
                .flatMap(course -> courseService.remove(id)
                                .then(Mono.fromCallable(() -> new ResponseEntity<Void>(HttpStatus.NO_CONTENT))))
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }

    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<Course>>> listByPage(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size) {
        return Mono.fromCallable(() -> PageRequest.of(page, size))
                .flatMap(courseService::findByPage)
                .map(pageSupportMono -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_STREAM_JSON)
                        .body(pageSupportMono))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

}
