package com.studentenrollment.exception;

import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(-1)
public class GlobalErrorExceptionHandler extends AbstractErrorWebExceptionHandler {

    public GlobalErrorExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ApplicationContext applicationContext, ServerCodecConfigurer configurer) {
        super(errorAttributes, resourceProperties, applicationContext);
        this.setMessageWriters(configurer.getWriters());
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    private Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        Map<String, Object> errorPropertiesMap = getErrorAttributes(request, false);
        Throwable error = getError(request);
        final Map<String, Object> mapException = new HashMap<>();
        var httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String status = String.valueOf(errorPropertiesMap.get("status"));

        if (error instanceof ConstraintViolationException) {
            status = "400";
        }

        switch (status) {
            case "400":
                mapException.put("error", "400");
                mapException.put("excepcion", "Parametros incorrectos");
                httpStatus = HttpStatus.BAD_REQUEST;
                break;

            default:
                mapException.put("error", "500");
                mapException.put("excepcion", "Error Interno");
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
        }

        mapException.put("mensaje", errorPropertiesMap.get("message"));
        mapException.put("ruta", request.uri());
        mapException.put("fecha-error", LocalDateTime.now());

        return ServerResponse.status(httpStatus)
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(BodyInserters.fromValue(mapException));
    }
}
