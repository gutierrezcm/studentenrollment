package com.studentenrollment.security;

import java.util.Date;

public class ErrorLogin {

	private String message;
	private Date timestamp;

	public ErrorLogin(String mensaje, Date timestamp) {		
		this.message = mensaje;
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String mensaje) {
		this.message = mensaje;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
