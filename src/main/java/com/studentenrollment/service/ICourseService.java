package com.studentenrollment.service;

import com.studentenrollment.document.Course;
import com.studentenrollment.pagination.PageSupport;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;

public interface ICourseService extends ICrud<Course, String> {

}
