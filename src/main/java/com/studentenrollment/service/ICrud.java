package com.studentenrollment.service;

import com.studentenrollment.pagination.PageSupport;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICrud<T, V> {

    Mono<T> save(T t);
    Mono<T> update(T t);
    Flux<T> findAll();
    Mono<T> findById(V v);
    Mono<Void> remove(V v);
    Mono<PageSupport<T>> findByPage(Pageable pageSupport);

}
