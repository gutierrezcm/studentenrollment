package com.studentenrollment.service;

import com.studentenrollment.document.UserDb;
import com.studentenrollment.security.User;
import reactor.core.publisher.Mono;

public interface IUserService extends ICrud<UserDb, String> {

    Mono<User> findByUsername(String username);

}
