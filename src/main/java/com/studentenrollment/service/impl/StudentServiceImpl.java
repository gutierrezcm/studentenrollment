package com.studentenrollment.service.impl;

import com.studentenrollment.document.Student;
import com.studentenrollment.pagination.PageSupport;
import com.studentenrollment.repository.IStudentRepo;
import com.studentenrollment.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements IStudentService {

    @Autowired
    private IStudentRepo studentRepo;

    @Override
    public Mono<Student> save(Student student) {
        return studentRepo.save(student);
    }

    @Override
    public Mono<Student> update(Student student) {
        return studentRepo.save(student);
    }

    @Override
    public Flux<Student> findAll() {
        return studentRepo.findAll();
    }

    @Override
    public Mono<Student> findById(String s) {
        return studentRepo.findById(s);
    }

    @Override
    public Mono<Void> remove(String s) {
        return studentRepo.deleteById(s);
    }

    @Override
    public Mono<PageSupport<Student>> findByPage(Pageable pageable) {
        return studentRepo.findAll()
                .collectList()
                .map(lista -> new PageSupport<>(
                        lista.stream()
                                .skip(pageable.getPageNumber() * pageable.getPageSize())
                                .limit(pageable.getPageSize())
                                .collect(Collectors.toList()),
                        pageable.getPageNumber(), pageable.getPageSize(), lista.size())
                );
    }
}
