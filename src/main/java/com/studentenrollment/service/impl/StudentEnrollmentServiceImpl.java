package com.studentenrollment.service.impl;

import com.studentenrollment.document.StudentEnrollment;
import com.studentenrollment.pagination.PageSupport;
import com.studentenrollment.repository.IStudentEnrollmentRepo;
import com.studentenrollment.service.IStudentEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class StudentEnrollmentServiceImpl implements IStudentEnrollmentService {

    @Autowired
    private IStudentEnrollmentRepo studentEnrollmentRepo;

    @Override
    public Mono<StudentEnrollment> save(StudentEnrollment studentEnrollment) {
        return studentEnrollmentRepo.save(studentEnrollment);
    }

    @Override
    public Mono<StudentEnrollment> update(StudentEnrollment studentEnrollment) {
        return studentEnrollmentRepo.save(studentEnrollment);
    }

    @Override
    public Flux<StudentEnrollment> findAll() {
        return studentEnrollmentRepo.findAll();
    }

    @Override
    public Mono<StudentEnrollment> findById(String s) {
        return studentEnrollmentRepo.findById(s);
    }

    @Override
    public Mono<Void> remove(String s) {
        return studentEnrollmentRepo.deleteById(s);
    }

    @Override
    public Mono<PageSupport<StudentEnrollment>> findByPage(Pageable pageable) {
        return studentEnrollmentRepo.findAll()
                .collectList()
                .map(lista -> new PageSupport<>(
                        lista.stream()
                                .skip(pageable.getPageNumber() * pageable.getPageSize())
                                .limit(pageable.getPageSize())
                                .collect(Collectors.toList()),
                        pageable.getPageNumber(), pageable.getPageSize(), lista.size())
                );
    }

}
