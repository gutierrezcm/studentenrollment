package com.studentenrollment.service.impl;

import com.studentenrollment.document.UserDb;
import com.studentenrollment.pagination.PageSupport;
import com.studentenrollment.repository.IUserRepo;
import com.studentenrollment.security.User;
import com.studentenrollment.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserRepo userRepo;

    @Override
    public Mono<User> findByUsername(String username) {
        List<String> roles = new ArrayList<String>();
        return userRepo.findOneByUser(username)
                .doOnNext(userDb -> {
                    userDb.getRoles().forEach(rol -> roles.add(rol.getName()));
                }).flatMap(userDb ->
                        Mono.fromCallable(() -> new User(userDb.getUser(), userDb.getPassword(),
                                                         userDb.getStatus(), roles))
                );
    }

    @Override
    public Mono<UserDb> save(UserDb userDb) {
        return userRepo.save(userDb);
    }

    @Override
    public Mono<UserDb> update(UserDb userDb) {
        return userRepo.save(userDb);
    }

    @Override
    public Flux<UserDb> findAll() {
        return userRepo.findAll();
    }

    @Override
    public Mono<UserDb> findById(String s) {
        return userRepo.findById(s);
    }

    @Override
    public Mono<Void> remove(String s) {
        return userRepo.deleteById(s);
    }

    @Override
    public Mono<PageSupport<UserDb>> findByPage(Pageable pageSupport) {
        return null;
    }
}
