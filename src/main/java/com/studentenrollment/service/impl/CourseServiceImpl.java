package com.studentenrollment.service.impl;

import com.studentenrollment.document.Course;
import com.studentenrollment.pagination.PageSupport;
import com.studentenrollment.repository.ICourseRepo;
import com.studentenrollment.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements ICourseService {

    @Autowired
    private ICourseRepo courseRepo;

    @Override
    public Mono<Course> save(Course course) {
        return courseRepo.save(course);
    }

    @Override
    public Mono<Course> update(Course course) {
        return courseRepo.save(course);
    }

    @Override
    public Flux<Course> findAll() {
        return courseRepo.findAll();
    }

    @Override
    public Mono<Course> findById(String s) {
        return courseRepo.findById(s);
    }

    @Override
    public Mono<Void> remove(String s) {
        return courseRepo.deleteById(s);
    }

    @Override
    public Mono<PageSupport<Course>> findByPage(Pageable pageable) {
        return courseRepo.findAll()
                .collectList()
                .map(lista -> new PageSupport<>(
                        lista.stream()
                                .skip(pageable.getPageNumber() * pageable.getPageSize())
                                .limit(pageable.getPageSize())
                                .collect(Collectors.toList()),
                        pageable.getPageNumber(), pageable.getPageSize(), lista.size())
                );
    }

}
